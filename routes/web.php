<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$router->get('/', function () use ($router) {
//    return $router->app->version();
//});


$router->get('/', function () use ($router) {
    return view('welcome');
});


$router->group(['prefix' => 'api/article'], function () use ($router) {

    $router->get('/','ArticleController@index');

    $router->get('/{id}','ArticleController@show');

    $router->post('/','ArticleController@store');

    $router->put('/{id}','ArticleController@update');

    $router->delete('/{id}','ArticleController@destroy');
});